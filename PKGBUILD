# Maintainer : Christian Hesse <mail@eworm.de>
# Maintainer : Tobias Powalowski <tpowa@archlinux.org>
# Contributor: Ronald van Haren <ronald.archlinux.org>
# Contributor: Keshav Amburay <(the ddoott ridikulus ddoott rat) (aatt) (gemmaeiil) (ddoott) (ccoomm)>

## "1" to enable IA32-EFI build in Arch x86_64, "0" to disable
_IA32_EFI_IN_ARCH_X64="1"

## "1" to enable EMU build, "0" to disable
_GRUB_EMU_BUILD="0"

[[ "${CARCH}" == 'x86_64' ]] && _EFI_ARCH='x86_64'
[[ "${CARCH}" == 'i686' ]] && _EFI_ARCH='i386'

[[ "${CARCH}" == 'x86_64' ]] && _EMU_ARCH='x86_64'
[[ "${CARCH}" == 'i686' ]] && _EMU_ARCH='i386'

pkgbase=grub
pkgname=('grub' 'update-grub' 'install-grub')
pkgdesc='GNU GRand Unified Bootloader (2)'
_tag='03e6ea18f6f834f177cad017279bedbb0a3de594' # git rev-parse grub-${_pkgver}
_pkgver=2.12
_unifont_ver='16.0.01'
pkgver=${_pkgver/-/}
pkgrel=7
url='https://www.gnu.org/software/grub/'
arch=('x86_64')
license=('GPL-3.0-or-later')
backup=('etc/default/grub'
        'etc/grub.d/40_custom')
options=('!makeflags')

conflicts=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}" 'grub-legacy')
replaces=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}")
provides=('grub-common' 'grub-bios' 'grub-emu' "grub-efi-${_EFI_ARCH}")

makedepends=('git' 'rsync' 'xz' 'freetype2' 'ttf-dejavu' 'python' 'autogen'
             'texinfo' 'help2man' 'gettext' 'device-mapper' 'fuse3')
depends=('sh' 'xz' 'gettext' 'device-mapper')
optdepends=(
  'dosfstools: For grub-mkrescue FAT FS and EFI support'
  'efibootmgr: For grub-install EFI support'
  'freetype2: For grub-mkfont usage'
  'fuse3: For grub-mount usage'
  'libisoburn: Provides xorriso for generating grub rescue iso using grub-mkrescue'
  'lzop: For grub-mkrescue LZO support'
  'mtools: For grub-mkrescue FAT FS support'
  'os-prober: To detect other OSes when generating grub.cfg in BIOS systems'
)


if [[ "${_GRUB_EMU_BUILD}" == "1" ]]; then
    makedepends+=('libusbx' 'sdl')
    optdepends+=('libusbx: For grub-emu USB support'
                 'sdl: For grub-emu SDL support')
fi

source=("git+https://git.savannah.gnu.org/git/grub.git#tag=${_tag}"
        'git+https://git.savannah.gnu.org/git/gnulib.git'
        "https://ftp.gnu.org/gnu/unifont/unifont-${_unifont_ver}/unifont-${_unifont_ver}.bdf.gz"
        '0001-00_header-add-GRUB_COLOR_-variables.patch'
        '0002-10_linux-detect-archlinux-initramfs.patch'
        '0003-support-dropins-for-default-configuration.patch'
        'grub.default'
        'update-grub'
        'install-grub'
        'install-grub.conf'
        'update-grub.hook'
        'install-grub.hook'
        'sbat.csv'
        'debian-blacklist-1440x900x32.patch'
        'debian-ppc64el-disable-vsx.patch'
        'debian-default-grub-d.patch'
        'debian-grub.cfg-400.patch'
        'arch-mkconfig-lunaos-distributor.patch')

sha256sums=('304d5c0d506d3aaab496bdcbfc325b89fc83348aeb9e69d7b6a928f4051bedb2'
            'SKIP'
            '230a0959aa50778b68239c88ad3c2d53abde58be0932b14a379a3869118aca33'
            '5dee6628c48eef79812bb9e86ee772068d85e7fcebbd2b2b8d1e19d24eda9dab'
            '8488aec30a93e8fe66c23ef8c23aefda39c38389530e9e73ba3fbcc8315d244d'
            'b5d9fcd62ffb3c3950fdeb7089ec2dc2294ac52e9861980ad90a437dedbd3d47'
            '7369d611cb5277b640115d0ce79c5ea16beee1d3ab5b3ca6cf66cc6667010c78'
            'c9027a993fe19a023bc6560aaee21487d97388d7997ba02db5c947bd0a5bdc12'
            'fa6d5ae6b8f8e126bd6811d4d8f947739fbc95400333740344e2f0b9f27b184a'
            '26c0482fb80651519cc49af0c646e3a5ab612b61b9bdc08c2a4558d452b31435'
            'a97ddf6694fa5070463a2d3f997205436a63fbe125071dd1bef0d59999adff70'
            '34f182499792f888e657b624f02837fab8762ca74e1a56884af8bf4fe2c33680'
            '53f0dbb58e2b4e85a5f212162c9bf2114753e38d12fb4c3e09aee1ea47737587'
            '7afefd1e0afb21924e2207cc644f6fd30b8cab7364fe24864df1bf64e054d61d'
            '61f422db46a9642656cbaebaad674a58952c25e4ab0dc5f93e20be4a9fc250c4'
            '33ef7e232cef526b4ab5e801f7423ff44ef0b4cbaf129da510edc92ffd4ca223'
            '95d7a502e81330268e05b8fb0fd9021f48b44f9d843a5b24197c15f272ae6c5e'
            '70935a07f80dd591cd7561e62ebc949a92ca32e04cd818e080fa665cab9ecbf5')

_configure_options=(
	PACKAGE_VERSION="${pkgver}-${pkgrel}"
	FREETYPE="pkg-config freetype2"
	BUILD_FREETYPE="pkg-config freetype2"
	--enable-nls
	--enable-device-mapper
	--enable-cache-stats
	--enable-grub-mkfont
	--enable-grub-mount
	--prefix="/usr"
	--bindir="/usr/bin"
	--sbindir="/usr/bin"
	--mandir="/usr/share/man"
	--infodir="/usr/share/info"
	--datarootdir="/usr/share"
	--sysconfdir="/etc"
	--program-prefix=""
	--with-bootdir="/boot"
	--with-grubdir="grub"
	--disable-silent-rules
	--disable-werror
)

prepare() {
	cd "${srcdir}/grub/"

	echo "Patches from Debian"
	patch -Np1 -i "${srcdir}/debian-blacklist-1440x900x32.patch"
	patch -Np1 -i "${srcdir}/debian-ppc64el-disable-vsx.patch"
	patch -Np1 -i "${srcdir}/debian-default-grub-d.patch"
	patch -Np1 -i "${srcdir}/debian-grub.cfg-400.patch"

	echo "Patch to enable GRUB_COLOR_* variables in grub-mkconfig..."
	## Based on http://lists.gnu.org/archive/html/grub-devel/2012-02/msg00021.html
	patch -Np1 -i "${srcdir}/0001-00_header-add-GRUB_COLOR_-variables.patch"

	echo "Patch to detect of Arch Linux initramfs images by grub-mkconfig..."
	patch -Np1 -i "${srcdir}/0002-10_linux-detect-archlinux-initramfs.patch"

	echo "Patch to support dropins for default configuration..."
	patch -Np1 -i "${srcdir}/0003-support-dropins-for-default-configuration.patch"

	echo "Patch to show LunaOS"
	patch -Np1 -i "${srcdir}/arch-mkconfig-lunaos-distributor.patch"

	echo "Fix DejaVuSans.ttf location so that grub-mkfont can create *.pf2 files for starfield theme..."
	sed 's|/usr/share/fonts/dejavu|/usr/share/fonts/dejavu /usr/share/fonts/TTF|g' -i "configure.ac"

	echo "Fix mkinitcpio 'rw' FS#36275..."
	sed 's| ro | rw |g' -i "util/grub.d/10_linux.in"

	echo "Fix OS naming FS#33393..."
	sed 's|GNU/Linux|Linux|' -i "util/grub.d/10_linux.in"

	echo "Pull in latest language files..."
	./linguas.sh

	echo "Avoid problem with unifont during compile of grub..."
	# http://savannah.gnu.org/bugs/?40330 and https://bugs.archlinux.org/task/37847
	gzip -cd "${srcdir}/unifont-${_unifont_ver}.bdf.gz" > "unifont.bdf"

	echo "Run bootstrap..."
	./bootstrap \
		--gnulib-srcdir="${srcdir}/gnulib/" \
		--no-git

	echo "Make translations reproducible..."
	sed -i '1i /^PO-Revision-Date:/ d' po/*.sed
}

_build_grub-common_and_bios() {
	echo "Set ARCH dependent variables for bios build..."
	if [[ "${CARCH}" == 'x86_64' ]]; then
		_EFIEMU="--enable-efiemu"
	else
		_EFIEMU="--disable-efiemu"
	fi

	echo "Copy the source for building the bios part..."
	cp -r "${srcdir}/grub/" "${srcdir}/grub-bios/"
	cd "${srcdir}/grub-bios/"

	echo "Unset all compiler FLAGS for bios build..."
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	echo "Run ./configure for bios build..."
	./configure \
		--with-platform="pc" \
		--target="i386" \
		"${_EFIEMU}" \
		--enable-boot-time \
		"${_configure_options[@]}"

	if [ ! -z "${SOURCE_DATE_EPOCH}" ]; then
		echo "Make info pages reproducible..."
		touch -d "@${SOURCE_DATE_EPOCH}" $(find -name '*.texi')
	fi

	echo "Run make for bios build..."
	make
}

_build_grub-efi() {
	echo "Copy the source for building the ${_EFI_ARCH} efi part..."
	cp -r "${srcdir}/grub/" "${srcdir}/grub-efi-${_EFI_ARCH}/"
	cd "${srcdir}/grub-efi-${_EFI_ARCH}/"

	echo "Unset all compiler FLAGS for ${_EFI_ARCH} efi build..."
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	echo "Run ./configure for ${_EFI_ARCH} efi build..."
	./configure \
		--with-platform="efi" \
		--target="${_EFI_ARCH}" \
		--disable-efiemu \
		--enable-boot-time \
		"${_configure_options[@]}"

	echo "Run make for ${_EFI_ARCH} efi build..."
	make
}

_build_grub-emu() {
	echo "Copy the source for building the emu part..."
	cp -r "${srcdir}/grub/" "${srcdir}/grub-emu/"
	cd "${srcdir}/grub-emu/"

	echo "Unset all compiler FLAGS for emu build..."
	unset CFLAGS
	unset CPPFLAGS
	unset CXXFLAGS
	unset LDFLAGS
	unset MAKEFLAGS

	echo "Run ./configure for emu build..."
	./configure \
		--with-platform="emu" \
		--target="${_EMU_ARCH}" \
		--enable-grub-emu-usb=no \
		--enable-grub-emu-sdl=no \
		--disable-grub-emu-pci \
		"${_configure_options[@]}"

	echo "Run make for emu build..."
	make
}

build() {
	cd "${srcdir}/grub/"

	echo "Build grub bios stuff..."
	_build_grub-common_and_bios

	echo "Build grub ${_EFI_ARCH} efi stuff..."
	_build_grub-efi

	if [[ "${CARCH}" == "x86_64" ]] && [[ "${_IA32_EFI_IN_ARCH_X64}" == "1" ]]; then
		echo "Build grub i386 efi stuff..."
		_EFI_ARCH="i386" _build_grub-efi
	fi

	if [[ "${_GRUB_EMU_BUILD}" == "1" ]]; then
		echo "Build grub emu stuff..."
		_build_grub-emu
	fi
}

_package_grub-common_and_bios() {
	cd "${srcdir}/grub-bios/"

	echo "Run make install for bios build..."
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	echo "Remove gdb debugging related files for bios build..."
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/i386-pc"/{kernel.exec,gdb_grub,gmodule.pl} || true

	echo "Install /etc/default/grub (used by grub-mkconfig)..."
	install -D -m0644 "${srcdir}/grub.default" "${pkgdir}/etc/default/grub"
}

_package_grub-efi() {
	cd "${srcdir}/grub-efi-${_EFI_ARCH}/"

	echo "Run make install for ${_EFI_ARCH} efi build..."
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	echo "Remove gdb debugging related files for ${_EFI_ARCH} efi build..."
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EFI_ARCH}-efi"/{kernel.exec,gdb_grub,gmodule.pl} || true

	sed -e "s/%PKGVER%/${pkgver}-${pkgrel}/" < "${srcdir}/sbat.csv" > "${pkgdir}/usr/share/grub/sbat.csv"
}

_package_grub-emu() {
	cd "${srcdir}/grub-emu/"

	echo "Run make install for emu build..."
	make DESTDIR="${pkgdir}/" bashcompletiondir="/usr/share/bash-completion/completions" install

	echo "Remove gdb debugging related files for emu build..."
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/*.module || true
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/*.image || true
	rm -f "${pkgdir}/usr/lib/grub/${_EMU_ARCH}-emu"/{kernel.exec,gdb_grub,gmodule.pl} || true
}


package_grub() {
	install="${pkgname}.install"
	cd "${srcdir}/grub/"

	echo "Package grub ${_EFI_ARCH} efi stuff..."
	_package_grub-efi

	if [[ "${CARCH}" == "x86_64" ]] && [[ "${_IA32_EFI_IN_ARCH_X64}" == "1" ]]; then
		echo "Package grub i386 efi stuff..."
		_EFI_ARCH="i386" _package_grub-efi
	fi

	if [[ "${_GRUB_EMU_BUILD}" == "1" ]]; then
		echo "Package grub emu stuff..."
		_package_grub-emu
	fi

	echo "Package grub bios stuff..."
	_package_grub-common_and_bios
}

package_update-grub() {
	pkgdesc="GNU Grub (2) Update Menu Script"
	depends=(grub)
	optdepends=()
	provides=()
	conflicts=('grub-update' 'grub-hook')
	backup=()
	echo "Install update-grub"
	install -Dm755 "${srcdir}/update-grub" "${pkgdir}/usr/bin/update-grub"
	echo "Install 99-update-grub.hook"
	install -D -m644 "${srcdir}/update-grub.hook" "${pkgdir}/usr/share/libalpm/hooks/99-update-grub.hook"
}

package_install-grub() {
	pkgdesc="GNU Grub (2) Install Script on Updates"
	depends=(coreutils efibootmgr gawk grep grub)
	optdepends=()
	provides=()
	conflicts=()
	replaces=()
	backup=('etc/install-grub.conf')
	echo "Install install-grub"
	install -Dm755 "${srcdir}/install-grub" "${pkgdir}/usr/bin/install-grub"
	echo "Install install-grub.conf"
	install -Dm644 "${srcdir}/install-grub.conf" "${pkgdir}/etc/install-grub.conf"
	echo "Install 98-install-grub.hook"
	install -Dm644 "${srcdir}/install-grub.hook" "${pkgdir}/usr/share/libalpm/hooks/98-install-grub.hook"
}

